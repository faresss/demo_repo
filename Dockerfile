FROM node:12.22-buster
WORKDIR /App
COPY package.json /App/
RUN npm install
COPY . /App
EXPOSE 3000
CMD [ "npm", "run", "start"]